const fs=require('fs')
const path=require('path')

function deleteFiles(file,cb){
    fs.unlink(file,(err)=>{
        cb(err)
    })
}

function fileRead(file,cb){
    fs.readFile(file,'utf-8',(err,data)=>{
        cb(err,data)
    })
}

function fileWrite(file,data,cb){
    fs.writeFile(file,data,(err)=>{
        cb(err)
    })
}

function fileAppend(file,data,cb){
    fs.appendFile(file,data,(err)=>{
        cb(err)
    })
}

function createandDeleteFiles(filePath){
    fileRead(filePath,(err,fileData)=>{
        if(err){
            console.error(err)
        }
        else{
            let upperCaseData=fileData.toUpperCase()
            fileWrite('upperCaseFile.txt',upperCaseData,(err)=>{
                if(err){
                    console.error(err)
                }
                else{
                    fileWrite('filename.txt','upperCaseFile.txt\n',(err)=>{
                        if(err){
                            console.error(err)
                        }
                        else{
                            let lowerCaseData=upperCaseData.toLowerCase().split('.')
                            let sentenceFile='sentenceFile.txt'
                            lowerCaseData.forEach(sentence => {
                                fileAppend(sentenceFile,`${sentence}`,(err)=>{
                                    if(err){
                                        console.error(err)
                                        return
                                    }
                                })
                                
                            });
                        fileAppend('filename.txt',`${sentenceFile}`,(err)=>{
                            if(err){
                                console.error(err)
                                return
                            }
                        })
                        fileRead('filename.txt',(err,fileData)=>{
                            if(err){
                                console.error(err)
                            }
                            else{
                                fileData=fileData.split('\n').filter((file)=>file!=='')
                                fileData.forEach((fileName)=>{
                                    fileRead(fileName,(err,Data)=>{
                                        if(err){
                                            console.error(err)
                                        }
                                        else{
                                            Data=Data.split(' ')
                                            Data.sort((x,y)=>{
                                                if(x>y){
                                                    return 1
                                                }
                                                else if(x<y){
                                                    return -1
                                                }
                                                else{
                                                    return 0
                                                }
                                            })
                                            Data=Data.toString()
                                            sortedFile=`sorted${fileName}`
                                            fileWrite(sortedFile,Data,(err)=>{
                                                if(err){
                                                    console.error(err)
                                                }
                                                else{
                                                    console.log('new sorted'+fileName)
                                                }
                                                fileAppend('/home/dilshad/project/fs-callback/test/filename.txt',`${sortedFile}`,(err)=>{
                                                    if(err){
                                                        console.error(err)
                                                        return
                                                    }
                                                })
                                            })
                                        }
                                    })
                                })
                        }
                        fs.readFile('/home/dilshad/project/fs-callback/test/filename.txt','utf-8',(err,fileContent)=>{
                            if(err){
                                console.error(err)
                            }
                            else{
                                let files=fileContent.split('\n').filter((file)=>file!='')
                                files.forEach((file)=>{
                                    deleteFiles(file,(err)=>{
                                        console.log(file)
                                        if(err){
                                            console.error(err)
                                        }
                                        else{
                                            console.log(file+'deleted successfully')
                                        }
                                    })
                                })
                            }
                        })
                        })
                    }
                        })
                    }
module.exports=createandDeleteFiles