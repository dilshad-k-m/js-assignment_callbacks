const fs=require('fs')
const path=require('path')

function createDirectory(dirName){
    let dirPath=path.join(__dirname,dirName)
    fs.mkdir(dirPath,(err)=>{
        if(err){
            console.error(err)
        }
    })
    console.log('directory created')
    return dirPath
}

function createFiles(dirPath,numberofFiles){
    for(let i=1;i<=numberofFiles;i++){
        let file=Math.random().toString().slice(3,5)+'.json'
        let filePath=path.join(dirPath,file)
        fs.writeFile(filePath,'Hi,iam Dilshad',(err)=>{
            if(err){
                console.error(err)
            }
        })
    }
    console.log('files created')
    return dirPath
}

function deleteFiles(dirPath){
    fs.readdir(dirPath,(err,data)=>{
        if(err){
            console.error(err)
        }
        else{
            data.forEach((fileName)=>{
                const filePath=path.join(dirPath,fileName)
                fs.unlink(filePath,(err)=>{
                    if(err){
                        console.error(err)
                        return
                    }
                })
            })
        }
    })
    console.log('deleted the files')
}
module.exports={createDirectory,createFiles,deleteFiles}